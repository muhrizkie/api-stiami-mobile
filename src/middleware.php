<?php

use \Slim\Middleware\JwtAuthentication;

// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

$app->add(new JwtAuthentication([
    "path" => "/sso", /* or ["/api", "/admin"] */
    "attribute" => "payload",
    "secret" => $container->get('settings')['jwt']['secret'],
    "algorithm" => ["HS256"],
    "error" => function ($request, $response, $arguments) {
        $data["error"] = true;
        $data["message"] = $arguments["message"];
        return $response
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    },
    "callback" => function ($request, $response, $arguments) use ($app) {
        $token = $arguments['token'];
        $sql = "SELECT * FROM cyberuserlist WHERE refreshtoken= :token";
        $sth = $app->getContainer()->db->prepare($sql);
        $sth->bindParam("token", md5($token));
        $sth->execute();
        $isValid = $sth->fetchObject();
        return $isValid != null;
    }
]));
