<?php

include "routes/agenda.php";
include "routes/berita.php";
include "routes/cabang.php";
include "routes/download.php";
include "routes/galeri.php";
include "routes/jurusan.php";
include "routes/home.php";
include "routes/infodaftar.php";
include "routes/ormawa.php";
include "routes/keunggulan.php";
include "routes/profile.php";
include "routes/event.php";
include "routes/sso.php";

// use Slim\Http\Request;
// use Slim\Http\Response;

// middleware validasi request harus json
$app->add(function ($request, $response, $next) {
    $mediaType = $request->getMediaType();
    if($mediaType == 'application/json'){
        return $next($request, $response);
    }else{
        return $response->withJson(["error" => "System Error" . $mediaType], 415);
    }
});