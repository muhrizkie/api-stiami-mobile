<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/Agenda', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM tbl_agenda WHERE publish = 'Y' ORDER BY tanggal DESC, jam DESC LIMIT 0,3");
    $query->execute();
    $data = $query->fetchAll();
    return $this->response->withJson($data);
});
