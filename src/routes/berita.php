<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/Berita', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT tbl_berita.*, tbl_berita_ktg.nama_berita_ktg, tbl_berita_ktg.aktif FROM tbl_berita LEFT JOIN tbl_berita_ktg ON tbl_berita.id_berita_ktg = tbl_berita_ktg.id_berita_ktg WHERE publish = 'Y' ORDER BY tanggal DESC LIMIT 0,3");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        $data[$key]['thumbnail'] = "https://stiami.ac.id/theme-assets/images/news/" . $value['id_berita'] . ".jpg";
    }
    return $this->response->withJson($data);
});
