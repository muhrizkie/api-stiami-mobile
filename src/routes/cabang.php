<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/Cabang', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM cabang ORDER BY kodecabang");
    $query->execute();
    $data = $query->fetchAll();
    foreach($data as $key => $value){
        $loc = explode(",", $value['latlong']);
        $data[$key]["location"]["latitude"] = $loc[0];
        $data[$key]["location"]["longitude"] = $loc[1];
        if($value['gambarkampus'] == ""){
            $data[$key]['gambarkampus'] = "stiami.img/default_background.jpg";
        }
        $data[$key]['gambarkampus'] = $data[$key]['gambarkampus'] . "?_t=" . filemtime($_SERVER['DOCUMENT_ROOT'] . '/' . $data[$key]['gambarkampus']);
    }
    return $this->response->withJson($data, 200, JSON_NUMERIC_CHECK);
});
