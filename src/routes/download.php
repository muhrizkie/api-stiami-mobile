<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/Download', function (Request $request, Response $response, array $args) {
    // get list download
    $query = $this->db->prepare("SELECT * FROM tbl_download ORDER BY id_download DESC");
    $query->execute();
    $data = $query->fetchAll();
    // get list files
    foreach($data as $key => $value){
        $query2 = $this->db->prepare("SELECT * FROM tbl_download_file WHERE id_download = :iddownload");
        $query2->bindParam("iddownload", $value['id_download']);
        $query2->execute();
        $data2 = $query2->fetchAll();
        $data[$key]["list_files"] = $data2;
    }
    return $this->response->withJson($data);
});
