<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/GreetingEvent', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * from greetingevent WHERE CURRENT_DATE <= expired_date ORDER BY idgreeting DESC LIMIT 0, 5");
    $query->execute();
    $data = $query->fetchAll();
    $returnData = $data[0];
    if($returnData != null){
      $returnData['gambar_event'] = $returnData['mobileimage'] . "?_t=" . filemtime($_SERVER['DOCUMENT_ROOT'] . '/' . $returnData['mobileimage']);
    }
    return $this->response->withJson($returnData);
});
