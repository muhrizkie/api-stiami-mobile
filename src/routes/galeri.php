<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/Galeri', function (Request $request, Response $response, array $args) {
    // get list download
    $query = $this->db->prepare("SELECT * FROM tbl_galeri_album");
    $query->execute();
    $data = $query->fetchAll();
    // get list files
    foreach($data as $key => $value){
        $query2 = $this->db->prepare("SELECT * FROM tbl_galeri_file WHERE id_galeri = :idgaleri");
        $query2->bindParam("idgaleri", $value['id_galeri']);
        $query2->execute();
        $data2 = $query2->fetchAll();
        $data[$key]["list_files"] = $data2;
    }
    return $this->response->withJson($data);
});
