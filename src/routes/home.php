<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/SliderInfo', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_sliderinfo WHERE status=1");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        $data[$key]['gambar'] = $data[$key]['gambar'] . "?_t=" . filemtime($_SERVER['DOCUMENT_ROOT'] . '/' . $data[$key]['gambar']);
    }
    return $this->response->withJson($data);
});

$app->get('/HomeBackground', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_backgroundmobile WHERE status=1");
    $query->execute();
    $data = $query->fetchAll();
    return $this->response->withJson($data[0]);
});
