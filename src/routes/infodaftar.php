<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/InfoDaftar', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_infodaftar WHERE status=1");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(!empty($value['url'])) {
            $data[$key]['url'] = $value['url'] . "?_t=" . strtotime(date('Y-M-d H:i:s'));
        }
    }
    array_push($data, [
        "id" => "pendaftaran",
        "menu" => "Form Pendaftaran",
        "deskripsi" => "",
        "status" => "1",
        "icon" => "account-question",
        "icon_type" => "MaterialCommunityIcons",
        "url" => "https://pmb.stiami.ac.id/daftar/m/indexmobile.php?_t=" . strtotime(date('Y-M-d H:i:s'))
    ]);
    return $this->response->withJson($data);
});