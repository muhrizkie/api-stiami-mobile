<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/Jurusan', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM jurusan");
    $query->execute();
    $data = $query->fetchAll();
    $newData = [
        "vokasi" => [
            "label" => "Program Siap Kerja / Terapan",
            "featuredImg" => "stiami.img/vokasi.jpg",
            "kampus" => [],
            "data" => [],
            "color" => "#03a200"
        ],
        "fia" => [
            "label" => "Program Sarjana (Ilmu Administrasi)",
            "featuredImg" => "stiami.img/fia.jpg",
            "kampus" => [],
            "data" => [],
            "color" => "#000aa3"
        ],
        "fisma" => [
            "label" => "Program Sarjana (Ilmu Sosial & Manajemen)",
            "featuredImg" => "stiami.img/fisma.jpg",
            "kampus" => [],
            "data" => [],
            "color" => "#ff9000"
        ],
        "pascasarjana" => [
            "label" => "Program Pascasarjana (S2)",
            "featuredImg" => "stiami.img/s2.jpg",
            "kampus" => [],
            "data" => [],
            "color" => "#ff0006"
        ]
    ];
    foreach($data as $key => $value) {
        $value["programstudi"] = str_replace(')', ' ', str_replace('(', '', $value["jenjang"])) . $value["prodi"];
        switch($value["kategori"]){
            default:
                array_push($newData["vokasi"]["data"], $value);
                break;
            case "2":
                array_push($newData["fia"]["data"], $value);
                break;
            case "3":
                array_push($newData["fisma"]["data"], $value);
                break;
            case "4":
                array_push($newData["pascasarjana"]["data"], $value);
                break;
        }
    }
    return $this->response->withJson($newData);
});
