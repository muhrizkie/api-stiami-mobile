<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/Keunggulan', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_slider");
    $query->execute();
    $data = $query->fetchAll();
    $base = $request->getUri()->getBaseUrl();
    foreach($data as $key => $value){
        $data[$key]["icon"] = $base . "/../../" . $value['icon'];
        $data[$key]["background"] = $base . "/../../" . $value['background'];
    }
    return $this->response->withJson($data);
});