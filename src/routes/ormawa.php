<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/Ormawa', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM profil_ormawa");
    $query->execute();
    $data = $query->fetchAll();
    foreach($data as $key => $value){
        $query2 = $this->db->prepare("SELECT * FROM detail_profil_ormawa where idkategori_ormawa = " . $value['id']);
        $query2->execute();
        $data2 = $query2->fetchAll();
        foreach($data2 as $key => $value) {
            $data2[$key]['organisasi'] = $value['menukategori'];
        }
        $data[$key]["organisasi"] = $data2;
    }
    return $this->response->withJson($data);
});