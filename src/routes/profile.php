<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/Profil', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data);
});

$app->get('/Profil/PesanRektor', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil where id=1");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data[0]);
});

$app->get('/Profil/Transformasi', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil where id=2");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data[0]);
});

$app->get('/Profil/VisiMisi', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil where id=3");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data[0]);
});

$app->get('/Profil/Budaya', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil where id=4");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data[0]);
});

$app->get('/Profil/Manajemen', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil where id=5");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data[0]);
});

$app->get('/Profil/Filosofi', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil where id=6");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data[0]);
});

$app->get('/Profil/Hymne', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil where id=7");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data[0]);
});

$app->get('/Profil/Profesor', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil where id=8");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data[0]);
});

$app->get('/Profil/Dosen', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil where id=9");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data[0]);
});

$app->get('/Profil/Sarana', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare("SELECT * FROM mob_profil where id=10");
    $query->execute();
    $data = $query->fetchAll();
    foreach ($data as $key => $value) {
        if(empty($data[$key]['foto'])){
            if(strpos(strtolower($data[$key]['menuprofil']), 'rektor') !== false){
                $data[$key]['foto'] = 'stiami.img/rektor_stiami.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/rektor_stiami.jpg');
            } else {
                $data[$key]['foto'] = 'stiami.img/default_background.jpg?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/stiami.img/default_background.jpg');
            }
        } else {
            $data[$key]['foto'] = $data[$key]['foto'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . $data[$key]['foto']);
        }
    }
    return $this->response->withJson($data[0]);
});