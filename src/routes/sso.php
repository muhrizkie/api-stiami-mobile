<?php

use Slim\Http\Request;
use Slim\Http\Response;
use \Firebase\JWT\JWT;

$app->post('/login[/{refreshToken}]', function (Request $request, Response $response, array $args) {
    if ($args['refreshToken']) {
        $sql = "SELECT * FROM cyberuserlist WHERE refreshtoken= :token";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("token", $args['refreshToken']);
        $sth->execute();
        $user = $sth->fetchObject();
    } else {
        $input = $request->getParsedBody();
        $sql = "SELECT * FROM cyberuserlist WHERE username= :uname AND `password`= :pwd";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("uname", $input['username']);
        $sth->bindParam("pwd", $input['password']);
        $sth->execute();
        $user = $sth->fetchObject();
    }

    // verify user.
    if (!$user) {
        return $this->response->withJson(['error' => true, 'message' => 'These credentials do not match our records.'], 401);
    }

    // get biodata
    $sql = "SELECT * FROM biodata WHERE nim= :uname";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("uname", $user->username);
    $sth->execute();
    $biodata = $sth->fetchObject();

    $secret = $this->get('settings')['jwt']['secret']; // get settings array.
    $token = JWT::encode(array_merge(
        array_map("utf8_encode", (array) $user),
        array_map("utf8_encode", (array) $biodata),
        (array) [
            'iat' => strtotime(date("Y-m-d H:i:s")),
            'exp' => strtotime(date("Y-m-d H:i:s", strtotime("+30 minutes"))),
        ]
    ), $secret, 'HS256');
    $refreshToken = md5($token);

    // save refresh token
    $sql = "UPDATE cyberuserlist SET refreshtoken = :token WHERE username = :uname";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("token", $refreshToken);
    $sth->bindParam("uname", $user->username);
    $sth->execute();

    return $this->response->withJson([
        'accessToken' => $token,
        'refreshToken' => $refreshToken,
    ]);
});

/**
 * Controller /data/type/query
 * - type: kodepos, kelurahan, kecamatan, kabkota, provinsi
 * * it's a public API
 */
$app->get('/data/{type}/{query}', function (Request $request, Response $response, array $args) {
    switch ($args['type']) {
        default:
            $selection = 'Kodepos';
            $groupBy = 'Kodepos';
            $where = 'Kodepos';
            break;
        case 'kelurahan':
            $selection = 'Id_Kelurahan, Kelurahan';
            $groupBy = 'Id_Kelurahan';
            $where = 'Kelurahan';
            break;
        case 'kecamatan':
            $selection = 'Id_Kecamatan, Kecamatan';
            $groupBy = 'Id_Kecamatan';
            $where = 'Kecamatan';
            break;
        case 'kabkota':
            $selection = 'Jenis, `Id_Kab/Kota`, `Kabupaten/Kota`';
            $groupBy = '`Id_Kab/Kota`';
            $where = '`Kabupaten/Kota`';
            break;
        case 'provinsi':
            $selection = 'Id_Provinsi, Provinsi';
            $groupBy = '`Id_Provinsi`';
            $where = '`Provinsi`';
            break;
    }
    $sql = "SELECT " . $selection . " FROM provinsi WHERE " . $where . " LIKE :query GROUP BY " . $groupBy;
    $sth = $this->db->prepare($sql);
    $query = "%" . $args['query'] . "%";
    $sth->bindParam("query", $query);
    $sth->execute();
    $results = $sth->fetchAll();

    return $this->response->withJson($results);
});

$app->group('/sso', function (\Slim\App $app) {
    /**
     * Controller /sso/profile
     * ambil profil user + menu dashboard
     */
    $app->get('/profile', function (Request $request, Response $response, array $args) {
        $user = $request->getAttribute('payload');

        $sql = "SELECT biodata.*, aplikan.*, biodata.kelas as Kelas, if((`biodata`.`Sex` = 1),'Laki-laki','Perempuan') AS `jeniskelamin`, ";
        $sql .= "if((`mobistiami_dummy_dbasemobile`.`biodata`.`Agama` = 1),'Islam',if((`mobistiami_dummy_dbasemobile`.`biodata`.`Agama` = 2),'Kristen Protestan',if((`mobistiami_dummy_dbasemobile`.`biodata`.`Agama` = 3),'Kristen Katolik',if((`mobistiami_dummy_dbasemobile`.`biodata`.`Agama` = 4),'Hindu',if((`mobistiami_dummy_dbasemobile`.`biodata`.`Agama` = 5),'Budha','Kong Hu Cu'))))) AS `agama`, ";
        $sql .= "if((`aplikan`.`kelas` = 1),'Baru','Pindahan') AS `jenismhs`, ";
        $sql .= "cabang.namacabang, jurusan.* FROM biodata JOIN aplikan ON biodata.ID = aplikan.id";
        $sql .= " JOIN jurusan ON jurusan.kodejurusan = biodata.kode";
        $sql .= " JOIN cabang ON cabang.kodecabang = biodata.kodecabang";
        $sql .= " WHERE biodata.nim= :uname";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("uname", $user->nim);
        $sth->execute();
        $payload = $sth->fetchObject();

        // get dashboard menu
        $sql = "SELECT * FROM mob_dashboard_menu_group WHERE active = 1 AND grp = :grp";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("grp", $user->grp);
        $sth->execute();
        $menus = $sth->fetchAll();
        for ($i = 0; $i < count($menus); $i++) {
            $sql = "SELECT * FROM mob_dashboard_menu WHERE `grp`= :grp AND group_menu = :group AND active = 1";
            $sth = $this->db->prepare($sql);
            $sth->bindParam("grp", $user->grp);
            $sth->bindParam("group", $menus[$i]['id']);
            $sth->execute();
            $submenus = $sth->fetchAll();
            for ($x = 0; $x < count($submenus); $x++) {
                if (!strpos(strtolower($submenus[$x]['icon']), 'http://') && !strpos(strtolower($submenus[$x]['icon']), 'https://')) {
                    $submenus[$x]['icon'] = str_replace($request->getUri()->getBasePath(), '/', $request->getUri()->getBaseUrl()) . $submenus[$x]['icon'] . '?_t=' . filemtime($_SERVER['DOCUMENT_ROOT'] . '/' . $submenus[$x]['icon']);
                }
            }
            $menus[$i]['submenus'] = $submenus;
        }
        $payload->menus = $menus;
        if ($payload->fotoaplikan && trim($payload->fotoaplikan) != '') {
            $payload->fotoaplikan = 'https://sso.stiami.ac.id/AdminMarketing/timthumb.php?w=256&zc=0&src=' . trim($payload->fotoaplikan);
        }

        // get all available semester
        // $sql = "SELECT semester FROM jadualmaster WHERE kodekelas = :kls GROUP BY semester";
        // $sth = $this->db->prepare($sql);
        // $sth->bindParam("kls", $payload->Kelas);
        // $sth->execute();
        // $semesters = $sth->fetchAll();
        // $payload->semesters = $semesters;

        return $this->response->withJson($payload);
    });

    /**
     * Controller update user profile in Aplikan & Biodata
     * untuk update profile
     */
    $app->put('/profile', function (Request $request, Response $response, array $args) {
        $user = $request->getAttribute('payload');
        $input = $request->getParsedBody();

        $requiredData = [
            "noidentitas",
            "Alamat",
            "kabupatenkota",
            "kodepos",
            "email",
            "HP",
            "Ortu",
            "alamatortu",
            "agamaayah",
            "Pekerjaan",
            "telportu",
            "pendidikanayah",
            "agamaibu",
            "pekerjaanibu",
            "telportuibu",
            "pendidikanibu",
            "namakantor",
            "alamatkantor",
            "kotakantor",
            "kodeposkantor",
            "telpkantor",
            "webkantor"
        ];

        if (empty($input)) {
            return $this->response->withJson(["error" => true, "message" => "New values are empty"], 406);
        }
        $this->db->beginTransaction();
        $sql = "UPDATE aplikan SET ";
        foreach ($requiredData as $key => $value) {
            if ($value == 'kodepos') {
                $sql .= '`KdPos` = :KdPos';
            } else {
                $sql .= '`' . $value . '` = :' . $value;
            }
            if ($key < count($requiredData) - 1) $sql .= ', ';
        }
        $sql .= " WHERE id = :id";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("id", $user->ID);
        foreach ($requiredData as $key => $value) {
            if ($value == 'kodepos') {
                $sth->bindParam('KdPos', $input[$value]);
            } else {
                $sth->bindParam($value, $input[$value]);
            }
        }
        $sth->execute();
        $aplikan = $sth->rowCount();

        $sql = "UPDATE biodata SET Alamat = ?, kodepos = ?, email = ? WHERE ID = ?";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(1, $input['Alamat']);
        $sth->bindParam(2, $input['kodepos']);
        $sth->bindParam(3, $input['email']);
        $sth->bindParam(4, $user->ID);
        $sth->execute();
        $biodata = $sth->rowCount();

        if ($aplikan > 0 || $biodata > 0) {
            $this->db->commit();
            return $this->response->withJson(["error" => false, "message" => "Data successfully updated"]);
        }
        $this->db->rollBack();
        return $this->response->withJson(["error" => false, "message" => "Nothing changed"]);
    });

    /**
     * controller change password
     * {
     *  oldpassword: '',
     *  newpassword: '',
     * }
     */
    $app->put('/profile/password', function (Request $request, Response $response, array $args) {
        $user = $request->getAttribute('payload');
        $input = $request->getParsedBody();

        if ($user->password != $input['oldpassword']) {
            return $this->response->withJson(["error" => true, "message" => "Incorrect old password"], 403);
        }

        $sql = "UPDATE cyberuserlist SET `password` = ? WHERE username = ?";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(1, $input['newpassword']);
        $sth->bindParam(2, $user->username);
        $sth->execute();
        $changed = $sth->rowCount();

        if ($changed > 0) {
            return $this->response->withJson(["error" => false, "message" => "Password changed successfully"]);
        }

        return $this->response->withJson(["error" => false, "message" => "Nothing changed"]);
    });

    /**
     * Controller /sso/jadwal/{semester}/{kelas}
     * Ambil list jadwal berdasarkan semester dan kelas
     */
    $app->get('/jadwal/{semester}/{kelas}', function (Request $request, Response $response, array $args) {
        $sql = "SELECT jadualmaster.*, matakuliahprodi.namamk, matakuliahprodi.sks FROM jadualmaster LEFT JOIN matakuliahprodi ON matakuliahprodi.kodemtk = jadualmaster.Kodemtk WHERE semester = :semester AND kodekelas = :kelas";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("kelas", $args['kelas']);
        $sth->bindParam("semester", $args['semester']);
        $sth->execute();
        $jadwal = $sth->fetchAll();

        return $this->response->withJson($jadwal);
    });

    /**
     * Controller /sso/info[/{size}/{page}]
     * Untuk info kampus
     * NOTE: Not implemented yet
     */
    $app->get('/info[/{size}/{page}]', function (Request $request, Response $response, array $args) {
        return $this->response->withJson([]);
    });

    /**
     * Controller modul data biaya kuliah
     */
    $app->get('/bayaran/{semester}', function (Request $request, Response $response, array $args) {
        $user = $request->getAttribute('payload');
        $sql = "SELECT *, (uangkuliah-potongan-diskon) as akumulasi, ((uangkuliah-potongan-diskon) - pinjaman) as sisabiaya FROM biayakuliah WHERE tahunakademik = :semester AND nim = :nim";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("nim", $user->nim);
        $sth->bindParam("semester", $args['semester']);
        $sth->execute();
        $data = $sth->fetchObject();

        $sql = "SELECT *, (terbayar-sebesar) as tunggakan FROM biaya WHERE ta = :semester AND nim = :nim";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("nim", $user->nim);
        $sth->bindParam("semester", $args['semester']);
        $sth->execute();
        $data->rencanaBayar = $sth->fetchAll();

        $sql = "SELECT *, (jumlahbayar + denda) as totalbayar FROM bayarkuliah WHERE tahunakademik = :semester AND nim = :nim";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("nim", $user->nim);
        $sth->bindParam("semester", $args['semester']);
        $sth->execute();
        $data->buktiBayar = $sth->fetchAll();

        $sql = "SELECT tahunakademik FROM biayakuliah WHERE nim = :nim";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("nim", $user->nim);
        $sth->execute();
        $data->options = $sth->fetchAll();

        return $this->response->withJson($data);
    });

    /**
     * Controller modul KRS by semester
     */
    $app->get('/krs/{semester}', function (Request $request, Response $response, array $args) {
        $user = $request->getAttribute('payload');

        $data = (object) [];

        $sql = "SELECT semester FROM viewkrs WHERE nim = :nim GROUP BY semester";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("nim", $user->nim);
        $sth->execute();
        $data->options = $sth->fetchAll();

        $sql = "SELECT * FROM viewkrs WHERE semester = :semester AND nim = :nim";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("nim", $user->nim);
        $sth->bindParam("semester", $args['semester']);
        $sth->execute();
        $data->details = $sth->fetchAll();

        $data->totalSks = 0;
        foreach ($data->details as $value) {
            $data->totalSks += $value['sks'];
        }

        return $this->response->withJson($data);
    });

    /**
     * Controller Kemajuan Nilai
     */
    $app->get('/nilai[/{semester}]', function (Request $request, Response $response, array $args) {
        $user = $request->getAttribute('payload');

        $data = (object) [];

        if (!isset($args['semester'])) {
            $sql = "SELECT SUM(sks) as totalSks, SUM(Kumulatif) as totalKumulatif, ROUND(SUM(Kumulatif) / SUM(sks), 2) as ipk FROM nilai WHERE nim = :nim";
            $sth = $this->db->prepare($sql);
            $sth->bindParam("nim", $user->nim);
            $sth->execute();
            $data = $sth->fetchObject();

            $sql = "SELECT semester FROM nilai WHERE nim = :nim GROUP BY semester";
            $sth = $this->db->prepare($sql);
            $sth->bindParam("nim", $user->nim);
            $sth->execute();
            $data->semesters = $sth->fetchAll();
        } else {
            $sql = "SELECT SUM(sks) as totalSks, SUM(Kumulatif) as totalKumulatif, ROUND(SUM(Kumulatif) / SUM(sks), 2) as ips FROM nilai WHERE nim = :nim AND semester = :semester";
            $sth = $this->db->prepare($sql);
            $sth->bindParam("nim", $user->nim);
            $sth->bindParam("semester", $args['semester']);
            $sth->execute();
            $data = $sth->fetchObject();

            $sql = "SELECT (sebesar-terbayar) as tunggakan FROM biaya WHERE ta = :semester AND nim = :nim AND MONTH(tglrencana) = MONTH(NOW())";
            $sth = $this->db->prepare($sql);
            $sth->bindParam("nim", $user->nim);
            $sth->bindParam("semester", $args['semester']);
            $sth->execute();
            $data->tunggakan = $sth->fetchObject();
            $data->tunggakan = intval($data->tunggakan->tunggakan);

            if ($data->tunggakan <= 0) {
                $sql = "SELECT nilai.*, matakuliahprodi.namamk, namemk FROM nilai JOIN matakuliahprodi ON nilai.kodemtk = matakuliahprodi.kodemtk WHERE nim = :nim AND semester = :semester";
                $sth = $this->db->prepare($sql);
                $sth->bindParam("nim", $user->nim);
                $sth->bindParam("semester", $args['semester']);
                $sth->execute();
                $data->komponen = $sth->fetchAll();
            } else {
                $data->komponen = [];
            }

            $sql = "SELECT ROUND(SUM(Kumulatif) / SUM(sks), 2) as ipk FROM nilai WHERE nim = :nim AND semester <= :semester";
            $sth = $this->db->prepare($sql);
            $sth->bindParam("nim", $user->nim);
            $sth->bindParam("semester", $args['semester']);
            $sth->execute();
            $data->ipk = $sth->fetchObject()->ipk;
        }

        if (floatval($data->ipk) > 3.6) {
            $data->kategori = 'CUM LAUDE';
            $data->kategoriWarna = '#D4AF37';
        } else if (floatval($data->ipk) > 3) {
            $data->kategori = 'MEMUASKAN';
            $data->kategoriWarna = '#ffcc33';
        } else if (floatval($data->ipk) > 2.5) {
            $data->kategori = 'CUKUP';
            $data->kategoriWarna = '#EA8402';
        } else if (floatval($data->ipk) > 1.5) {
            $data->kategori = 'SANGAT CUKUP';
            $data->kategoriWarna = '#FB851E';
        } else {
            $data->kategori = 'TIDAK LULUS';
            $data->kategoriWarna = '#F85A48';
        }

        return $this->response->withJson($data);
    });
});
